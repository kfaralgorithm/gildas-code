<?php

namespace Database\Seeders;

use App\Models\Hiring;
use Faker\Factory as Faker;
use Illuminate\Support\Str;
use App\Models\HiringAbility;
use Illuminate\Database\Seeder;
use App\Models\HiringResponsibility;
use App\Models\HiringRequiredProfile;
use App\Models\HiringRequiredDocument;

class HiringSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        foreach (range(1, 25) as $i) {

            $t =  time() . "-" . $i;

            $date = rand(0, 1) == 0 ? now() : $faker->iso8601;

            $hiring = Hiring::create([
                'poste' => "Poste " . $t,
                'description' => $faker->paragraph,
                'statut' => rand(0, 1),
                'type' => ['CDI', 'CDD'][rand(0, 1)],
                'created_at' => $date,
                'updated_at' => $date,
            ]);

            foreach (range(1, 5) as $k) {
                HiringAbility::create([
                    'content' => "Ability " . $t . "-" . $k,
                    'hiring_id' => $hiring->id,
                    'created_at' => $date,
                    'updated_at' => $date,
                ]);
            }

            foreach (range(1, 10) as $j) {
                HiringResponsibility::create([
                    'content' => "Responsability " . $t . "-" . $j,
                    'hiring_id' => $hiring->id,
                    'created_at' => $date,
                    'updated_at' => $date,
                ]);
            }

            foreach (range(1, 11) as $l) {
                HiringRequiredProfile::create([
                    'content' => "Profil required " . $t . "-" . $l,
                    'hiring_id' => $hiring->id,
                    'created_at' => $date,
                    'updated_at' => $date,
                ]);
            }

            foreach (range(1, 6) as $o) {
                HiringRequiredDocument::create([
                    'content' => "Document required " . $t . "-" . $o,
                    'hiring_id' => $hiring->id,
                    'created_at' => $date,
                    'updated_at' => $date,
                ]);
            }
        }

        $hirings = Hiring::all();

        foreach ($hirings as $key => $hiring) {
            $hiring->slug = Str::slug($hiring->poste . " " . $hiring->created_at);
            $hiring->save();
        }
    }
}
