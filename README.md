<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Installation

1. Clonez le projet,
2. Ouvrez le projet dans le terminal et éxecutez ``composer install``,
3. Créez un fichier ``.env`` à la racine du dossier créé par le clonage avec le meme contenu que le fichier ``.env.example``,
4. Créez une base de donnée dans un sgbd (``Postgres, MySQL ou autres``),
5. Configurez les attributs liés à la base de donnée dans le fichier ``.env`` (ils commencent par ``DB_``),
6. Exécutez la commande ``php artisan migrate --seed`` ou successivement les deux commandes ``php artisan migrate`` et ``php artisan db:seed``,
7. Démarrez le server avec la commande ``php artisan serve``.



## Ressource

- [Laravel installation](https://laravel.com/docs/8.x/installation).
- [Laravel configuration](https://laravel.com/docs/8.x/configurations).
- [Laravel routing engine](https://laravel.com/docs/routing).
- [Laravel controller](https://laravel.com/docs/8.x/controllers).
