<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('hirings.index');
});


Route::get('/hirings', [\App\Http\Controllers\HiringController::class, 'index'])->name('hirings.index');
Route::get('/hirings/{slug}', [\App\Http\Controllers\HiringController::class, 'show'])->name('hirings.show');
Route::get('/hirings/{slug}/apply', [\App\Http\Controllers\HiringController::class, 'createApply'])->name('hirings.apply.create');
Route::post('/hirings/{slug}/apply', [\App\Http\Controllers\HiringController::class, 'apply'])->name('hirings.apply');
