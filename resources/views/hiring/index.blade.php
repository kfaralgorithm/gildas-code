@extends('default')

@section('title', 'Liste')

@section('css')

    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }

    </style>

@endsection


@section('content')
    <header class="pb-3 mb-4 border-bottom">
        <a href="/" class="d-flex align-items-center text-dark text-decoration-none">
            <span class="fs-4">Avis de recrutement</span>
        </a>
    </header>

    <div class="container py-5">
        <div class="row">
            <div class="col-md-9 col-sm-12">
                <table class="table table-bordered mb-5">
                    <thead class="table-primary">
                        <tr>
                            <th scope="col">Date de publication</th>
                            <th scope="col">Statut</th>
                            <th scope="col">Fonction</th>
                        </tr>
                    </thead>
                    <tbody>

                        @if ($hirings->isEmpty())
                            {{-- Si on a 0 element --}}

                            <tr>
                                <td colspan="3">
                                    <div class="d-flex justify-content-center">0 élement trouvé</div>
                                </td>
                            </tr>

                        @else
                            {{-- Si on a plus d'un --}}
                            @foreach ($hirings as $data)
                                <tr>
                                    <th scope="row">{{ $data->publication_at }}</th>
                                    <td>{{ $data->statut == 0 ? 'En cours' : 'Cloturé' }}</td>
                                    <td>
                                        <div class="d-flex justify-content-between">
                                            <span>{{ $data->poste }}</span>
                                            <a href="{{ route('hirings.show', ['slug' => $data->slug]) }}"
                                                class="btn btn-outline-primary">DETAILS</a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>


                {{-- Pagination --}}
                {{-- https://laravel.com/docs/8.x/pagination#paginator-instance-methods --}}

                @if ($hirings->total() != 0)
                    <div class="d-flex justify-content-between">
                        <span>{{ $hirings->count() }}/{{ $hirings->total() }}
                            élement{{ $hirings->total() != 1 ? 's' : '' }}
                            trouvé{{ $hirings->total() != 1 ? 's' : '' }}</span>

                        {{-- "{!! $hirings->links() !!}" génere le code de pagination. Sa configuration a été fait dans en ajoutant  "Paginator::useBootstrap();"
                         dans la fonction boot contenu dans le fichier app/Providers/AppServiceProvider.php --}}
                        {!! $hirings->links() !!}
                    </div>
                @endif
            </div>
            <div class="
                            col-md-3 col-sm-12">
                <form method="GET" action="{{ route('hirings.index') }}">
                    @csrf
                    {{-- Notation de securisation du formulaire --}}
                    <div class="mb-3">
                        <label for="rechercheinput" class="form-label">Recherche</label>

                        <div class="input-group mb-3">
                            <input type="text" class="form-control" id="rechercheinput" name="qs"
                                value="{{ $qs ?? '' }}" placeholder="Rechercher">
                            <span class="input-group-text" id="basic-addon2"><svg xmlns="http://www.w3.org/2000/svg"
                                    width="16" height="16" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                                    <path
                                        d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z" />
                                </svg></span>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-12 d-flex justify-content-end">
                            <button type="submit" class="btn btn-primary">RECHERCHER</button>
                        </div>
                    </div>
                </form>

                Filtrer par date:

                <div class="filtre">
                    <ul style="list-style-type: none;" class="p-0">
                        <li><a href="{{ route('hirings.index') }}?qd=new{{ $qs ? '&qs=' . $qs : '' }}">Récent</a>
                        </li>
                        <li><a href="{{ route('hirings.index') }}?qd=week{{ $qs ? '&qs=' . $qs : '' }}">Il y
                                a une
                                semaine</a></li>
                        <li><a href="{{ route('hirings.index') }}?qd=month{{ $qs ? '&qs=' . $qs : '' }}">Il
                                y a un
                                mois</a></li>
                        <li><a href="{{ route('hirings.index') }}?qd=lastmonth{{ $qs ? '&qs=' . $qs : '' }}">Le
                                mois
                                dernier</a></li>
                    </ul>
                </div>

            </div>
        </div>
    </div>

    <footer class="pt-3 mt-4 text-muted border-top">
        Gildas Code &copy; 2021
    </footer>

@endsection
