@extends('default')

@section('title', $hiring->poste)

@section('content')

    <header class="pb-3 mb-4 border-bottom d-flex justify-content-between">
        <a href="{{ route('hirings.show', ['slug' => $hiring->slug]) }}" class="text-dark text-decoration-none">
            <span class="fs-4">Candidature à l'avis de recrutement : {{ $hiring->poste }}</span>
        </a>
        <a href="{{ route('hirings.show', ['slug' => $hiring->slug]) }}" class="text-danger btn-xs pull-right">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-circle"
                viewBox="0 0 16 16">
                <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                <path
                    d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
            </svg>
        </a>
    </header>

    <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
        <symbol id="check-circle-fill" fill="currentColor" viewBox="0 0 16 16">
            <path
                d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z" />
        </symbol>
        <symbol id="exclamation-triangle-fill" fill="currentColor" viewBox="0 0 16 16">
            <path
                d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z" />
        </symbol>
    </svg>



    @if (Session::has('error'))

        <div class="alert alert-danger d-flex align-items-center" role="alert">
            <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:">
                <use xlink:href="#exclamation-triangle-fill" />
            </svg>
            <div>
                {{ Session::get('error') }}
            </div>
        </div>

    @endif

    @if (Session::has('success'))

        <div class="alert alert-success d-flex align-items-center" role="alert">
            <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:">
                <use xlink:href="#check-circle-fill" />
            </svg>
            <div>
                {{ Session::get('success') }}
            </div>
        </div>

    @endif


    <form class="row g-3" method="POST" action="{{ route('hirings.apply', ['slug' => $hiring->slug]) }}"
        enctype="multipart/form-data">
        @csrf
        <div class="col-md-6">
            <label for="name-input" class="form-label">Nom <sup class="text-danger"><strong>*</strong></sup></label>
            <input type="text" class="form-control" id="name-input" name="name_input">
        </div>
        <div class="col-md-6">
            <label for="surname-input" class="form-label">Prénoms <sup
                    class="text-danger"><strong>*</strong></sup></label>
            <input type="text" class="form-control" id="surname-input" name="surname_input" required>
        </div>
        <div class="col-md-6">
            <label for="email-input" class="form-label">Adresse email <sup
                    class="text-danger"><strong>*</strong></sup></label>
            <input type="email" class="form-control" id="email-input" name="email_input" required>
        </div>
        <div class="col-md-6">
            <label for="tel-input" class="form-label">Téléphone <sup
                    class="text-danger"><strong>*</strong></sup></label>
            <input type="text" class="form-control" id="tel-input" name="tel_input" required>
        </div>
        <div class="col-md-6">
            <label for="cv-input" class="form-label">Curriculum vitae <sup
                    class="text-danger"><strong>*</strong></sup></label>
            <input class="form-control" type="file" id="cv-input" name="cv_input" required>
        </div>
        <div class="col-md-6">
            <label for="last-d-input" class="form-label">Dernier diplome <sup
                    class="text-danger"><strong>*</strong></sup></label>
            <input class="form-control" type="file" id="last-d-input" name="last_d_input" required>
        </div>
        <div class="col-md-6">
            <label for="motivation-letter-input" class="form-label">Lettre de motivation <sup
                    class="text-danger"><strong>*</strong></sup></label>
            <input class="form-control" type="file" id="motivation-letter-input" name="motivation_letter_input" required>
        </div>
        <div class="col-12 d-flex justify-content-center">
            <button class="btn btn-danger" style="margin-right: 20px !important;">Annuler</button>
            <button type="submit" class="btn btn-primary">Envoyer</button>
        </div>
    </form>


@endsection
