@extends('default')

@section('title', $hiring->poste)

@section('content')

    <div class="container py-5">
        <ul class="list-group list-group-horizontal">
            <li class="list-group-item d-flex justify-content-between align-items-start border-0"
                style="margin-left: 0px !important; padding-left: 0px !important;">
                <div>
                    <div class="fw-bold">Poste</div>
                    {{ $hiring->poste }}
                </div>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-start border-0">
                <div class="ms-2 me-auto">
                    <div class="fw-bold">Type</div>
                    {{ $hiring->type }}
                </div>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-start border-0">
                <div class="ms-2 me-auto">
                    <div class="fw-bold">Document</div>
                    <a href="" class="btn btn-primary"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                            fill="currentColor" class="bi bi-eye" viewBox="0 0 16 16">
                            <path
                                d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.133 13.133 0 0 1 1.66-2.043C4.12 4.668 5.88 3.5 8 3.5c2.12 0 3.879 1.168 5.168 2.457A13.133 13.133 0 0 1 14.828 8c-.058.087-.122.183-.195.288-.335.48-.83 1.12-1.465 1.755C11.879 11.332 10.119 12.5 8 12.5c-2.12 0-3.879-1.168-5.168-2.457A13.134 13.134 0 0 1 1.172 8z" />
                            <path
                                d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0z" />
                        </svg> Visualiser</a>
                </div>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-start border-0">
                <div class="ms-2 me-auto">
                    <div class="fw-bold">Date de publication</div>
                    {{ $hiring->publication_at }}
                </div>
            </li>

            <a href="{{ route('hirings.index') }}" class="text-danger btn-xs pull-right">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-circle"
                    viewBox="0 0 16 16">
                    <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                    <path
                        d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
                </svg>
            </a>
        </ul>



        <span class="fw-bold">Description : </span>
        <p>
            {{ $hiring->description }}
        </p>

        <span class="fw-bold">Principales attributions : </span>
        <ul>
            @foreach ($hiring->responsabilities as $responsability)
                <li>{{ $responsability->content }}</li>
            @endforeach
        </ul>

        <span class="fw-bold">Profil requis : </span>
        <ul>
            @foreach ($hiring->profiles as $profil)
                <li>{{ $profil->content }}</li>
            @endforeach
        </ul>

        <span class="fw-bold">Aptitudes : </span>
        <ul>
            @foreach ($hiring->abilities as $ability)
                <li>{{ $ability->content }}</li>
            @endforeach
        </ul>

        <span class="fw-bold">Pièces à Fournir : </span>
        <ul>
            @foreach ($hiring->documents as $document)
                <li>{{ $document->content }}</li>
            @endforeach
        </ul>
        <div class="d-flex justify-content-center">
            <a href="{{ route('hirings.apply.create', ['slug' => $hiring->slug]) }}" class="btn btn-primary">Postuler</a>
        </div>
    </div>
@endsection
