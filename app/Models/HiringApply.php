<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class HiringApply extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = ['name', 'surname', 'email', 'phone', 'cv', 'last_d', 'motivation_letter'];
}
